﻿using _17_Events;
using _17_Events.Models;

var finder = new FileFinder(@"C:\temp");

finder.FileFound += Finder_OnFileFound;
finder.SearchEnded += Finder_OnSearchEnded;
Console.WriteLine("Нажмите Ctrl+C чтобы остановить поиск");

finder.Start();

finder.StoppedByUser = true;
finder.FileFound -= Finder_OnFileFound;
finder.SearchEnded -= Finder_OnSearchEnded;

Console.ReadLine();

static float StringToRandom(string element)
{
    Random rnd = new Random();
    return rnd.Next(100);
}

static float StringToLength(string element)
{
    return element.Length;
}

static float StringToELetters(string element)
{
    return element.Length - element.Replace("е","").Length;
}

static void Finder_OnFileFound(FileArgs Params)
{
    Console.WriteLine("File found: "+ Params.FileName);
}

static void Finder_OnSearchEnded(FileFinder Finder)
{
    Console.WriteLine("Search ended");

    Console.WriteLine($"Максимум по случайному алгоритму: {MaximumFinder<string>.GetMax(Finder.FoundFiles, StringToRandom)}");
    Console.WriteLine($"Максимум по длине пути: {MaximumFinder<string>.GetMax(Finder.FoundFiles, StringToLength)}");
    Console.WriteLine($"Максимум по количеству букв е: {MaximumFinder<string>.GetMax(Finder.FoundFiles, StringToELetters)}");
}