﻿using _17_Events.Models;

namespace _17_Events
{
    public class FileFinder
    {
        public string Directory { get; set; }
        public bool StoppedByUser { get; set; }
        public List<string> FoundFiles { get; set; }

        public event Action<FileArgs> FileFound;
        public event Action<FileFinder> SearchEnded;

        public FileFinder(string directory)
        { 
            Directory = directory;
            StoppedByUser = false;
            FoundFiles = new List<string>();

            Console.CancelKeyPress += new ConsoleCancelEventHandler(Stop);
        }

        public void Start()
        {
            GetFilesRecursive(Directory);
            if (!StoppedByUser) Stop();
        }

        public void Stop(object sender = null, ConsoleCancelEventArgs args = null)
        {
            StoppedByUser = true;
            Console.CancelKeyPress -= new ConsoleCancelEventHandler(Stop);
            SearchEnded.Invoke(this);
        }

        public void GetFilesRecursive(string Path)
        {
            if (SearchStopped()) return;

            DirectoryInfo pathInfo = new DirectoryInfo(Path);

            foreach (DirectoryInfo folderInfo in pathInfo.GetDirectories())
            {
                GetFilesRecursive(folderInfo.FullName);
            }

            foreach (FileInfo fileInfo in pathInfo.GetFiles())
            {
                if (SearchStopped()) break;

                FoundFiles.Add(fileInfo.FullName);
                FileFound?.Invoke(new FileArgs(fileInfo.FullName));
                Thread.Sleep(2000);
            }
        }

        private bool SearchStopped()
        {
            return StoppedByUser;
        }
    }
}
