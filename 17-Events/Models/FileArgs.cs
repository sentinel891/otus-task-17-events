﻿namespace _17_Events.Models
{
    public class FileArgs: EventArgs
    {
        public string FileName { get; set; }
        public FileArgs(string fileName)
        { 
            FileName = fileName;
        }
    }
}
