﻿namespace _17_Events
{
    public class MaximumFinder<T>
    {
        public delegate float MaximumFunction(T value);
        private record SearchElement(T element, float value);

        public static T GetMax(IEnumerable<T> elements, MaximumFunction maximumFunctionHandler)
        {
            return elements.OrderByDescending(el => maximumFunctionHandler(el)).FirstOrDefault();
        }
    }
}
